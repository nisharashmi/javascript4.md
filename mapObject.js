//const testObject = { name: 'Bruce Wayne' , age: 36, location: 'Gotham'};
cb = x => "maped: "+x;
function mapObject(obj, cb) {
    
    const mapped = {}
    for( let prop in obj ){
        mapped[prop] = cb(obj[prop]);
    }
    return mapped;
}
module.exports = mapObject;
//console.log(mapObject(testObject, cb));