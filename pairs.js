function pairs(obj) {
    const arr = [];
    for(let key in obj) {
        arr.push([key,obj[key]]);
    }
    return arr;
}
module.exports = pairs;
//console.log(pairs(testObject));